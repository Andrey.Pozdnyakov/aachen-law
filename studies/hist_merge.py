import numpy as np

from coffea import hist
import boost_histogram as bh
from itertools import product


np.random.seed(42)
test_pt = np.random.exponential(10.0, size=100_000) + np.random.exponential(10, size=100_000)


# coffea
sparses = ["a", "b", "c"]
sparse_coffea = hist.Hist(
    "pt", *(hist.Cat(sp, sp) for sp in sparses), hist.Bin("x", "x", 20, 0, 200)
)
empty_coffea = sparse_coffea.copy()

# fill
for x in product("abcdef", repeat=3):
    d = dict(zip(sparses, x))
    d.update({"x": test_pt})
    sparse_coffea.fill(**d)


# boost

sparse_boost = bh.Histogram(
    *(bh.axis.StrCategory(sp, growth=True) for sp in sparses), bh.axis.Regular(20, 0, 200)
)

empty_boost = sparse_boost.copy()

dummy_boost = sparse_boost.copy()

# fill
for x in product("defcba", repeat=3):
    dummy_boost.fill(*x, test_pt)

# fill
for x in product("abcdef", repeat=3):
    sparse_boost.fill(*x, test_pt)


"""
In [1]: %timeit empty_coffea.copy().add(sparse_coffea)
3.8 ms ± 219 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)

In [2]: %timeit empty_boost + sparse_boost
288 µs ± 7.8 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)

In [3]: %timeit dummy_boost + sparse_boost
862 µs ± 17.3 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)
"""

import pickle

"""
In [4]: %timeit pickle.dumps(sparse_coffea)
777 µs ± 30.1 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)

In [5]: %timeit pickle.dumps(sparse_boost)
43.7 µs ± 4.32 µs per loop (mean ± std. dev. of 7 runs, 10000 loops each)
"""


with open("coffea.pkl", "wb") as f:
    pickle.dump(sparse_coffea, f)

with open("boost.pkl", "wb") as f:
    pickle.dump(sparse_boost, f)


from IPython import embed

embed()
