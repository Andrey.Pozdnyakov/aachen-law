# coding: utf-8

from copy import copy
import re

import numpy as np
import awkward as ak
import vector

from tools.data import SKDict

from tasks.base import AnalysisTask
from utils.cern import CERNSession
from .base import Correction

"""
    CorrMET = coor(events, dataset)
"""


class METPhiCorrections(Correction, AnalysisTask):
    version = None
    url = "https://lathomas.web.cern.ch/lathomas/METStuff/XYCorrections/XYMETCorrection_withUL17andUL18.h"

    def run(self):
        url = self.analysis_inst.aux.get("metphi_corrections", self.url)

        with CERNSession() as s:
            corr = METPhiCorrector.from_raw(s.get(url).content.decode("utf8"))

        self.output().dump(corr)


class METPhiCorrector:
    re_val = re.compile(
        r"if\(runera==y(UL|)(\d+)([A-Z]+)\)MET([xy])corr=-\((-?\d+\.\d+)\*npv\+?(-?\d+\.\d+)\);"
    )

    @classmethod
    def from_raw(cls, raw, **kwargs):
        return cls(cls.parse(raw, **kwargs))

    @classmethod
    def parse(cls, raw):
        # skip header-ish stuff
        lines = iter(raw.splitlines())
        for line in lines:
            if line.strip().startswith("double MET"):
                break
        else:
            raise RuntimeError("did not find start marker: double MET")

        ret = SKDict()
        ctx = []
        inv = None
        for line in lines:
            line = line.split("//", 1)[0].strip().replace(" ", "")
            if not line:
                continue
            if line.startswith("doubleCorrectedMET"):
                break

            if m := cls.re_val.match(line):
                ul, year, run, xy, c1, c0 = m.groups()
                key = tuple(ctx) + (ul or "!UL", int(year), run, xy)
                ret[key] = dict(c0=float(c0), c1=float(c1))
            elif m := re.match(r"if\((!?\w+)\){", line):
                ctx.append(m.group(1))
            elif line == "else{":
                assert inv
                ctx.append(inv)
                inv = None
            elif line == "}":
                inv = ctx.pop()
                inv = inv[1:] if inv.startswith("!") else f"!{inv}"
            else:
                raise RuntimeError(f"unexpected line: {line}")
        else:
            raise RuntimeError("did not find stop marker: double CorrectedMET")

        return ret

    def __init__(self, data):
        self.data = data

    def __call__(self, met: ak.Array, npv: ak.Array, dataset, **keys) -> ak.Array:

        run = dataset.aux.get("run", "MC")
        year = int(dataset.campaign.aux["year"])

        keys.setdefault("usemetv2", False)
        keys.setdefault("ispuppi", False)
        keys.setdefault("UL", False)

        keys = tuple(k if v else f"!{k}" for k, v in keys.items() if isinstance(v, bool))
        dat = self.data[(run, year) + keys]

        npv = np.minimum(npv, 100)
        metVec = vector.awk(met) - vector.awk(
            {
                o: np.multiply(dat[o, "c1"], npv, dtype=met.pt.type.type.dtype) + dat[o, "c0"]
                for o in "xy"
            }
        )
        met = copy(met)
        met["pt"] = metVec.rho
        met["phi"] = metVec.phi

        return met
