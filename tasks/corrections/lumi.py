# coding: utf-8

from functools import partial

import numpy as np

from tasks.base import CampaignTask
from tasks.files import DownloadFilesCampaign

from .base import Correction


class LumiMaskCorrections(Correction, CampaignTask):
    version = None

    def requires(self):
        return DownloadFilesCampaign.req(self, type="default")

    def run(self):
        from coffea.lumi_tools import LumiMask
        from coffea.nanoevents.methods.base import NanoEventsArray
        from coffea.util import save

        def lumimask(json: str, events: NanoEventsArray) -> np.ndarray:
            return LumiMask(json)(events.run, events.luminosityBlock)

        # hack: make numba kernel pickable with partial
        out = partial(lumimask, self.input()["POG_lumi_file"].path)
        self.output().parent.touch()
        save(out, self.output().path)
