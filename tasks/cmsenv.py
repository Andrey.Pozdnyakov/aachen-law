# coding: utf-8

import os
from subprocess import check_call

import law

from tasks.base import CampaignTask
from tasks.files import DownloadFiles


class PileUpReweighting(CampaignTask):

    version = None
    n_bins = 100

    cwd = "/cvmfs/cms.cern.ch/cc8_amd64_gcc8/cms/cmssw/CMSSW_11_1_3/"
    exe = "bin/cc8_amd64_gcc8/pileupCalc.py"

    def output(self):
        return {dir: self.local_target("pu.%s.root" % dir) for dir in ["nominal", "up", "down"]}

    def requires(self):
        return DownloadFiles.req(self, type="default")

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    @law.decorator.localize
    def run(self):

        mbxs = self.campaign_inst.aux["min_bias_xsec"]
        pileup_file = self.input()["POG_pileup_file"].path
        lumi_file = self.input()["POG_lumi_file"].path

        for dir, tar in self.output().items():
            xs = mbxs.get(direction=dir)
            self.logger.info("calculating shift=%s xs=%.5s" % (dir, xs))
            # https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData#Pileup_JSON_Files_For_Run_II
            check_call(
                [
                    self.cwd + self.exe,
                    "-i",
                    lumi_file,
                    "--inputLumiJSON",
                    pileup_file,
                    "--minBiasXsec",
                    str(int(xs * 1000)),
                    "--maxPileupBin",
                    str(self.n_bins),
                    "--numPileupBins",
                    str(self.n_bins),
                    "--calcMode",
                    "true",
                    tar.path,
                ],
                env=dict(
                    os.environ,
                    PYTHONPATH=self.cwd + "python/",
                ),
            )
