# coding: utf-8

import hashlib
from functools import cached_property, wraps
from itertools import chain
from operator import itemgetter

import law
from luigi import BoolParameter, Parameter

from tasks.coffea import CoffeaProcessor
from utils.datacard import Datacard

years = [2016, 2017, 2018]


class RecipeMixin:
    recipe = CoffeaProcessor.recipe

    def store_parts(self):
        return super().store_parts() + (self.recipe,)


class ModelMixin:
    model = Parameter(default="nlo")
    model_version = Parameter(default="")
    statmodel = Parameter(default="StatModel")

    @cached_property
    def StatModel(self):
        model = self.analysis_inst.aux.get("stat_model", self.model)
        sm = getattr(self._import("models", model), self.statmodel)
        assert issubclass(sm, Datacard)
        return sm

    def store_parts(self):
        parts = super().store_parts() + (self.model,) + (self.statmodel,)
        if self.model_version:
            parts += (self.model_version,)
        return parts


class VariableMixin:
    variable = Parameter(default="", description="Variable to fit")

    @property
    def var(self):
        if self.variable:
            var = self.variable
        else:
            category_inst = self.analysis_inst.categories.get(self.category)
            var = category_inst.aux["fit_variable"]
            self.logger.info(f"variable for {self.category} set to fit_variable ({var})")
        return var

    def store_parts(self):
        return super().store_parts() + (self.var,)


class CategoryMixin:
    category = Parameter(default="all_incl_sr")

    def store_parts(self):
        return super().store_parts() + (self.category,)


class PGroupMixin:
    process_group = law.CSVParameter(default="default")

    @property
    def is_config_pgroup(self):
        return self.process_group[0] in self.process_groups

    @property
    def process_groups(self):
        return self.analysis_inst.aux["process_groups"]

    @cached_property
    def legacy_processes(self):
        if self.is_config_pgroup:
            return self.process_groups[self.process_group[0]]
        else:
            return self.process_group

    def getPGroot(self, process, process_group=None):
        if process_group is None:
            process_group = self.legacy_processes
        for _, p in process.walk_parent_processes(include_self=True):
            if any(p is self.analysis_inst.processes.get(pg) for pg in process_group):
                return p

    @property
    def legacy_processes_hash(self):
        return hashlib.sha256("".join(sorted(self.legacy_processes)).encode("utf-8")).hexdigest()

    def store_parts(self):
        return super().store_parts() + (self.legacy_processes_hash,)


class CreateIssueMixin:
    create_issue = BoolParameter()


class CombinedMixin:
    years = law.CSVParameter(default=["2016", "2017", "2018", "run2"])

    def store_parts(self):
        return super().store_parts() + (",".join(sorted(self.years)),)
