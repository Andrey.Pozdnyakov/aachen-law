#!/usr/bin/env bash

action() {
    local origin="$( /bin/pwd )"

    [ -z "$USER" ] && export USER="$( whoami )"

    #
    # global variables "DHA = Di-Higgs-Analysis"
    #

    export LD_LIBRARY_PATH=
    export PYTHONPATH=

    export DHA_BASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && /bin/pwd )"

    # check if were we are
    case "$( hostname -f )" in

        lxplus*.cern.ch)
        export DHA_ON_LXPLUS=1
        export DHA_DIST_VERSION="slc7"
        ;;

        vispa-*|vispa-*.physik.rwth-aachen.de)

        export DHA_ON_VISPA=1
        mkdir -p "$HOME/.config/dask"
        [ -e "$HOME/.config/dask/vispa.yml" ] || ln -s "$DHA_BASE/dask.yml" "$HOME/.config/dask/vispa.yml"
        export DHA_DATA="/net/scratch/cms/dihiggs"
        export DHA_DASK_CERT="/net/scratch/cms/dihiggs/tls_ca_file.pem"
        export DHA_DASK_KEY="/net/scratch/cms/dihiggs/tls_client_key.pem"
        export DASGOCLIENT="$(ls -1t /cvmfs/cms.cern.ch/cc?_amd64_*/cms/dasgoclient/*/bin/dasgoclient  | head -n 1)"
        software conda
        export DHA_DIST_VERSION="debian"
        unset HDF5_PLUGIN_PATH
        ulimit -f hard
        ;;

        lx3a*.physik.rwth-aachen.de)
        export DHA_DATA="/net/scratch_cms3a/dihiggs"
        export DHA_DIST_VERSION="slc7"
        ;;
    esac

    # setup user if file exists
    SETUP_USER="$DHA_BASE/setup_user.sh"
    if [ -f "$SETUP_USER" ]; then
        source "$SETUP_USER"
    fi


    # complain when no grid user is set
    true ${USER:?user name environment variable missing}
    true ${DHA_DATA:?data directory environment variable missing}
    true ${DHA_GRID_USER:?grid user name environment variable missing}
    if [ ! -d "${DHA_ANALYSIS_ID:?analysis id environment variable missing}" ]; then
        2>&1 echo "ERROR: analysis id \"$DHA_ANALYSIS_ID\" is invalid"
        return "1"
    fi

    (( $(umask) & 070 )) && echo "WARNING: umask should not mask group rights, fix by e.g.: 'umask 07'"

    # set defaults of other variables
    [ -z "$DHA_SOFTWARE" ] && export DHA_SOFTWARE="$DHA_DATA/software/$USER/$DHA_DIST_VERSION"
    [ -z "$DHA_STORE" ] && export DHA_STORE="$DHA_DATA/store"
    [ -z "$DHA_LOCAL_CACHE" ] && export DHA_LOCAL_CACHE="/tmp/dihiggs_cache/"
    [ -z "$DHA_GRID_VO" ] && export DHA_GRID_VO="cms:/cms/dcms"
    [ -z "$DHA_EOS" ] && export DHA_EOS="/eos/user/${DHA_GRID_USER:0:1}/$DHA_GRID_USER"

    if [[ ! -d "$DHA_LOCAL_CACHE" ]]; then
      mkdir -p "$DHA_LOCAL_CACHE"
      chmod g+rwXs "$DHA_LOCAL_CACHE"
      chgrp cms "$DHA_LOCAL_CACHE"
    fi

    #
    # law setup
    #

    # law and luigi setup
    export LAW_HOME="$DHA_BASE/.law"
    export LAW_CONFIG_FILE="$DHA_BASE/law.cfg"
    # export LAW_TARGET_TMP_DIR="$DHA_DATA/tmp"

    # variables that are used in law.cfg and depend on whether we run on the grid or not
    if [ "$DHA_ON_GRID" == "1" ]; then
        export DHA_LUIGI_WORKER_KEEP_ALIVE="False"
        export DHA_LUIGI_WORKER_FORCE_MULTIPROCESSING="True"
    else
        export DHA_LUIGI_WORKER_KEEP_ALIVE="True"
        export DHA_LUIGI_WORKER_FORCE_MULTIPROCESSING="False"
    fi

    # print a warning when no luigi scheduler host is set
    if [ -z "$DHA_SCHEDULER" ]; then
        2>&1 echo "NOTE: DHA_SCHEDULER is not set, use '--local-scheduler' in your tasks!"
    fi


    #
    # software setup
    #

    dha_install_software() {
        local mode="$1"

        # conda unavailable?
        if ! command -v conda &>/dev/null
        then
            local conda="$DHA_SOFTWARE/conda"
            # install conda (attempt)
            if [ ! -d "$conda" ]
            then
            local tmp=$(mktemp)
                curl "https://repo.anaconda.com/miniconda/Miniconda2-latest-Linux-x86_64.sh" -o $tmp
                chmod u+x $tmp
                $tmp -b -p "$conda" && echo "
auto_activate_base: false
verify_threads: 3
execute_threads: 3

envs_dirs:
  - $conda/envs
  - /net/scratch/cms/conda/envs
  - /net/\${USER}/conda/envs
  - ~/.conda/envs

pkgs_dirs:
  - $conda/conda/pkgs
  - /net/scratch/cms/conda/pkgs
  - /net/\${USER}/conda/pkgs
  - ~/.conda/pkgs
" > "$conda/condarc" || rm -rf "$conda"
                rm $tmp
            fi
            # try to activate installed conda (if available)
            [ -x "$conda/bin/conda" ] && eval "$($conda/bin/conda shell.$(basename $SHELL) hook)"
        fi
        # conda available?
        if command -v conda &>/dev/null
        then
            conda activate cms6 && return "0"
            conda activate cms5 && return "0"
            conda activate cms3 && return "0"
            conda activate cms2 && return "0"
            conda activate cms && return "0"
            conda activate dihiggs && return "0"
            conda env update && conda activate dihiggs && return "0"
        fi
    }
    export -f dha_install_software
    dha_install_software silent

    dha_list_cache_locks() {
        [ -d "$DHA_LOCAL_CACHE" ] && find "$DHA_LOCAL_CACHE" -name "*.lock" -print
    }
    export -f dha_list_cache_locks

    dha_release_cache_locks() {
        [ -d "$DHA_LOCAL_CACHE" ] && find "$DHA_LOCAL_CACHE" -name "*.lock" -delete
    }
    export -f dha_release_cache_locks

    dha_env() {
        env | grep -P "^DHA_" | column -t -s=
    }
    export -f dha_env

    # add _this_ repo
    export PYTHONPATH="$DHA_BASE:$PYTHONPATH"

    # add _this_ repo
    export PATH="$DHA_BASE:$PATH"

    # VERY IMPORTANT...
    export GLOBUS_THREAD_MODEL="none"

    # source law's bash completion scipt
    source "$( law completion )"
}
action "$@"
