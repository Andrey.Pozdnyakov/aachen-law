# coding: utf-8

from copy import copy
from dataclasses import dataclass
import re
from collections import namedtuple
from operator import attrgetter, mul, add
from typing import Generator, NewType, Optional, Tuple, Dict, Union, get_type_hints

import numpy as np
import awkward as ak
from awkward._connect._numpy import array_ufunc
from coffea.lookup_tools.jme_standard_function import jme_standard_function
from coffea.lookup_tools.jersf_lookup import jersf_lookup
from coffea.lookup_tools.jec_uncertainty_lookup import jec_uncertainty_lookup
from coffea.lookup_tools.lookup_base import lookup_base
from numpy.lib.npyio import BagObj


JERJets = namedtuple("JERJets", ["nominal", "up", "down"])
JERsf = Optional[NewType("JERsf", tuple)]


@ak.mixin_class(ak.behavior)
class JECJet:
    def scale(self, value):
        ret = copy(self)
        for key in ("JetPt", "JetE", "JetMass"):
            if hasattr(self, key):
                ret[key] = self[key] * value
                # ret[key] = ak.virtual(mul, args=(self[key], value), cache="new")
        return ret

    def addPt(self, deltaPt):
        return self.scale(
            ak.virtual(lambda dPt, jPt: 1 + dPt / jPt, (deltaPt, self.JetPt), cache="new")
        )

    def makeJER(self, stack, normals, forceStochastic=False, minEnergy=1e-2) -> JERsf:
        if not (stack.jer and stack.jersf):
            return None

        resolution = stack.jer.getResolution(self)
        sfs = stack.jersf.getScaleFactor(self)
        pt_gen = ak.zeros_like(self.JetPt) if forceStochastic else self.ptGenJet

        jersmear = resolution * normals
        deltaPtRel = (self.JetPt - pt_gen) / self.JetPt
        doHybrid = (pt_gen > 0) & (np.abs(deltaPtRel) < 3 * resolution)

        minFactor = minEnergy / self.JetE

        sf = []
        for i in range(3):  # nom, up, down
            detSmear = 1 + (sfs[..., i] - 1) * deltaPtRel
            stochSmear = 1 + np.sqrt(np.maximum(sfs[..., i] ** 2 - 1, 0)) * jersmear
            smearFactor = ak.where(doHybrid, detSmear, stochSmear)

            sf.append(np.maximum(smearFactor, minFactor))

        return tuple(sf)

    def applyJER(self, jer: JERsf) -> JERJets:
        if jer is None:
            return JERJets(self, self, self)
        else:
            return JERJets(*(self.scale(j) for j in jer))


JECInfo = namedtuple("JECInfo", ["campaign", "dataera", "datatype", "level", "jettype"])


class JECComponent:
    _func_type = jme_standard_function
    _levelre = re.compile(".+")
    _level_order = ()

    def _level_sort_key(self, level_func):
        level = self._levelre.search(level_func[0])[0]
        if level in self._level_order:
            return self._level_order.index(level)
        else:
            return len(self._level_order)

    def _split_name(self, name: str) -> JECInfo:
        parts = name.split("_")
        if re.match(r"V[0-9]+", parts[2]):
            parts[1:3] = [parts[1] + parts[2]]
        return JECInfo(*parts)

    def __init__(self, **kwargs):
        lvl2func = {}
        accinfo = {
            "campaign": None,
            "dataera": None,
            "datatype": None,
            "jettype": None,
        }

        for name, func in kwargs.items():
            if not isinstance(func, self._func_type):
                raise TypeError(f"{func} expected to be {self._func_type} but is {type(func)}")

            info = self._split_name(name)._asdict()
            level = info.pop("level")
            if len(self._levelre.findall(level)) != 1:
                raise KeyError(f"Level malformatted: {level}, expected one {self._levelre}")
            if level in lvl2func:
                raise KeyError(f"Level does already exists: {level}")
            lvl2func[level] = func
            for key, val in accinfo.items():
                new = info[key]
                if val is None:
                    accinfo[key] = new
                elif accinfo[key] != new:
                    raise RuntimeError(f"Inconsistent {key}: {val} != {new}!")

        for key, val in accinfo.items():
            if val is None:
                raise RuntimeError(f"Unable to determine {key} for {type(self).__name__}")

        if not lvl2func:
            raise RuntimeError("No Levels prodvided?!")

        lvl2func = dict(sorted(lvl2func.items(), key=self._level_sort_key))

        self.lvl2func = lvl2func
        self.info = accinfo

    @property
    def signature(self):
        return set().union(*(set(func.signature) for func in self.lvl2func.values()))

    @property
    def levels(self):
        """list the different sources of uncertainty"""
        return list(self.lvl2func.keys())

    def __repr__(self):
        info = dict(self.info, levels=", ".join(self.levels), signature=str(self.signature))
        kmax = max(map(len, info.keys()))
        info = "\n".join(f"\t{key:{kmax}}: {val}" for key, val in info.items())
        return f"{type(self).__name__}(\n{info}\n)"

    def evaluate(self, level, obj=None, *, lazy_cache=None, **kwargs) -> ak.Array:
        func = self.lvl2func[level]

        args = tuple(kwargs[key] if key in kwargs else getattr(obj, key) for key in func.signature)

        if any(isinstance(arg, ak.highlevel.Array) for arg in args):
            return array_ufunc(func, "__call__", args, {})
        elif all(isinstance(arg, np.ndarray) for arg in args):
            return func(*args)
        else:
            raise RuntimeError("Unknown array library for inputs.")


class FactorizedJetCorrector(JECComponent):
    _levelre = re.compile("[L1-7]+")
    _level_order = ["L1", "L2", "L3", "L2L3"]

    def correct_gen(
        self, obj: JECJet = None, *, lazy_cache=None
    ) -> Generator[Tuple[str, JECJet], None, None]:
        cumcorr = 1
        for level in self.levels:
            corr = self.evaluate(level=level, obj=obj, lazy_cache=lazy_cache)
            obj = obj.scale(corr)
            cumcorr = cumcorr * corr
            yield level, obj, cumcorr

    def correct(self, obj: JECJet = None, *, lazy_cache=None, upto=None) -> JECJet:
        for level, obj, _ in self.correct_gen(obj=obj, lazy_cache=lazy_cache):
            if level == upto:
                break
        return obj


class JetResolution(JECComponent):
    _levelre = re.compile("Resolution")
    _level_order = ["Resolution"]

    def getResolution(self, *args, **kwargs):
        return self.evaluate(self.levels[-1], *args, **kwargs)


class JetResolutionScaleFactor(JECComponent):
    _func_type = jersf_lookup
    _levelre = re.compile("SF+")
    _level_order = ["SF"]

    def getScaleFactor(self, *args, **kwargs):
        return self.evaluate(self.levels[-1], *args, **kwargs)


class JetCorrectionUncertainty(JECComponent):
    _func_type = jec_uncertainty_lookup

    def _split_name(self, name: str) -> JECInfo:
        parts = name.split("_")

        # Check for the case of regrouped jes uncertainties
        if "Regrouped" in parts[0]:
            parts.pop(0)
            if "UncertaintySources" in parts:
                subparts = parts[parts.index("UncertaintySources") :]
                if len(subparts) == 4:
                    parts[-2:] = [subparts[-2] + "_" + subparts[-1]]

        # Check for the case when the dataera name contains a _ like "17Nov2017_V6"
        if re.match(r"V[0-9]+", parts[2]):
            parts[1:3] = [parts[1] + parts[2]]

        if "UncertaintySources" in parts:
            parts[3] = parts.pop()

        if len(parts) != 5:
            raise RuntimeError(f"Corrector name {name} is not properly formatted!")

        # use a generic 'jes' for normal uncertainty
        parts[3] = parts[3].replace("Uncertainty", "jes")

        return JECInfo(*parts)

    def getUncertainty(self, *args, **kwargs) -> Dict[str, ak.highlevel.Array]:
        return {
            uncertainty: self.evaluate(uncertainty, *args, **kwargs) for uncertainty in self.levels
        }

    def correct(self, uncertainty, down=False, obj: JECJet = None, *, lazy_cache=None) -> JECJet:
        return obj.scale(
            self.evaluate(uncertainty, obj=obj, lazy_cache=lazy_cache)[..., (1 if down else 0)]
        )


@dataclass
class JECStack:
    jec: FactorizedJetCorrector
    junc: Optional[JetCorrectionUncertainty] = None
    jer: Optional[JetResolution] = None
    jersf: Optional[JetResolutionScaleFactor] = None

    @classmethod
    def assemble(cls, corrections: Dict[str, lookup_base], **kwargs) -> "JECStack":
        cargs = dict(jec={}, junc={}, jer={}, jersf={})

        for key, corr in corrections.items():
            if "Uncertainty" in key:
                ctype = "junc"
            elif "SF" in key:
                ctype = "jersf"
            elif "Resolution" in key and "SF" not in key:
                ctype = "jer"
            elif len(FactorizedJetCorrector._levelre.findall(key)) > 0:
                ctype = "jec"
            else:
                raise RuntimeError(f"could not determin correction type for {key}")

            cargs[ctype][key] = corr

        hints = get_type_hints(cls)
        for key, val in cargs.items():
            # process override
            if key in kwargs:
                cargs[key] = kwargs[key]
                continue

            typ = hints[key]

            # skip optional
            if not isinstance(typ, type):
                if not val:
                    cargs[key] = None
                    continue
                else:
                    typ = typ.__args__[0]

            if not val:
                raise RuntimeError(f"no {key} corrections given!")
            cargs[key] = typ(**val)

        return cls(**cargs)
