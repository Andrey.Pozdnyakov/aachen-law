# coding: utf-8


class Shape:

    # histogram naming template:
    # uproot does not support subdirectories yet...
    template: str = "{category}#{process}#{variable}#{systematic}"

    # template for extracting shapes for combine:
    ch_template_nom: str = "$BIN#$PROCESS#{variable}#nominal"
    ch_template_sys: str = "$BIN#$PROCESS#{variable}#$SYSTEMATIC"

    def __init__(self, name: str) -> None:
        self.extract(name)

    @property
    def name(self) -> str:
        return self._name

    @property
    def category(self) -> str:
        return self._properties["category"]

    @property
    def process(self) -> str:
        return self._properties["process"]

    @property
    def variable(self) -> str:
        return self._properties["variable"]

    @property
    def systematic(self) -> str:
        return self._properties["systematic"]

    def __str__(self) -> str:
        return f"Shape(category={self.category}, process={self.process}, variable={self.variable}, systematic={self.systematic})"

    def extract(self, name: str) -> None:
        assert len(name.split("#")) == 4
        self._name = name
        self._properties = {
            self.template.split("#")[i].strip("{}"): name.split("#")[i]
            for i in range(len(name.split("#")))
        }
