# coding: utf-8

import warnings

import utils.aci as aci
from config.processes import ALL

analysis = aci.Analysis("common")

analysis.processes.extend(ALL)


def get(*args, **kwargs):
    warnings.warn(
        "get() is deprecated, use analysis.processes.get() instead!",
        DeprecationWarning,
        stacklevel=2,
    )
    return analysis.processes.get(*args, **kwargs)


files = {
    "default": {
        "Tallinn_gf_hh_coeff": "https://github.com/cms-hh/HHStatAnalysis/raw/master/AnalyticalModels/data/coefficientsByBin_extended_3M_costHHSim_19-4.txt",
        "hh_coeff": "https://github.com/fabio-mon/HHStatAnalysis/raw/c8fc33d2ae3f7e04cfc83e773e2880657ffdce3b/AnalyticalModels/data/pm_pw_NLO_Ais_13TeV_V2.txt",
    }
}
